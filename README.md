### Reconfigure Docker in current terminal window
eval $(minikube docker-env)

### Set deployment image
kubectl set image deployments/**deploymentfile_metadata_name** **deploymentfile_container_name**=**repositoryImageUrl**

### Secret
kubectl create secret generic **secret_name** --from-literal **key**=**value**

### Create EKS cluster
eksctl create cluster --name **NAME** --version 1.14 --region us-east-2 --nodegroup-name standard-workers --node-type t3.medium --nodes 3 --nodes-min 1 --nodes-max 4


### Setting Up Kubernetes
Links:
- https://gitlab.com/dangordon/eks-utils
- https://edenmal.moe/post/2018/GitLab-Kubernetes-Using-GitLab-CIs-Kubernetes-Cluster-feature/
- https://docs.gitlab.com/ee/user/project/clusters/add_remove_clusters.html#add-existing-eks-cluster
- https://docs.aws.amazon.com/eks/latest/userguide/getting-started.html

Steps:
- Create EKS cluster above
- kubectl get secrets
- Get the Token kubectl get secret **secretName** -o yaml
- Get Secret (direct) kubectl -n kube-system describe secret $(kubectl -n kube-system get secret | grep eks-admin | awk '{print $1}') | grep ^token | awk -F"[ \t]+" '{print $2}'
- Get CA Certificate kubectl get secret default-token-87kjc -o jsonpath="{['data']['ca\.crt']}" | base64 -D
