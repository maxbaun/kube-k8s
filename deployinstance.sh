# !/bin/bash
die () {
    echo >&2 "$@"
    return
}

if [ -z "$1" ]; then
    die "No argument supplied"
fi

# . addcname.sh $1

helm install --name $1 ./bb/ --set secretName="bb$1" --set host=$1.mavenanalytics.dev

# helm template --name test ./bb/ --set secretName=bbttest --set host=test.mavenanalytics.dev --output-dir ./bin
# kubectl apply -f bin/bb/templates

