# !/bin/bash
die () {
    echo >&2 "$@"
    return
}

if [ -z "$1" ]; then
    die "No argument supplied"
fi

echo '
{
  "HostedZoneId": "ZKQIZW84PDOZT",
  "ChangeBatch": {
    "Comment": "CREATE/DELETE/UPSERT a record ",
    "Changes": [
      {
        "Action": "CREATE",
        "ResourceRecordSet": {
          "Name": "${BUSINESS}.mavenanalytics.dev",
          "Type": "CNAME",
          "TTL": 300,
          "ResourceRecords": [
            {
              "Value": "mavenanalytics.dev"
            }
          ]
        }
      }
    ]
  }
}
' | sed s/\${BUSINESS}/"$1"/ > ./bin/add.json

aws route53 change-resource-record-sets --cli-input-json file://bin/add.json || echo "Already record $t"

